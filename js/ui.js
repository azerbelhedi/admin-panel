function productBox({ name, quantity, price, picture, id }) {
    return `
    <div class="product-box" id = "${id}" >
        <img src="${picture}"
            alt="img">
        <div class="data-section-1"> ${name} </div>
        <div class="data-section-2"> ${price}  DT ( ${quantity}  Pieces)</div>
    </div>
    `;
}

function displayProducts(products) {
    let productsElement = document.querySelector("#products");

    productsElement.innerHTML = "";

    for (let i = 0; i < products.length; i++) {
        productsElement.innerHTML += productBox(products[i]);
    }
}

function getFormData() {
    let formData = {
        name: document.querySelector("#name").value,
        price: parseInt(document.querySelector("#price").value) + 100,
        quantity: document.querySelector("#quantity").value,
        picture: document.querySelector("#picture").value,
    };

    return formData;
}

function addProduct({ name, quantity, price, picture }) {
    let newProduct = {
        id: ++lastId,
        name: name,
        price: price,
        quantity: quantity,
        picture: picture,
    };
    products.push(newProduct);
}

function handleCreateButton() {
    console.log("create button");
    addProduct(getFormData());
    displayProducts(products);
}

function handleResetForm() {
    document.querySelector("#name").value = "";
    document.querySelector("#price").value = "";
    document.querySelector("#quantity").value = "";
    document.querySelector("#picture").value = "";
}
